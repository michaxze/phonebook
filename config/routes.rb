Rails.application.routes.draw do
  namespace :api, defaults: {format: 'json'} do
      resources :contacts
  end

  root 'contacts#index'
  resources :contacts do
    collection do
      get "download"
      get "upload"
      post "upload"
    end
  end

end
