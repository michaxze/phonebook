module ContactsHelper
  def show_number_type(current_type='')
    html = "<select name=\"contact[number_type][]\" class=\"form-control\" style=\"width:80px;\">"
      Contact::NUMBER_TYPES.keys.each do |k|
        selected = (k.to_s == current_type.to_s) ? 'selected' : ''
        html += "<option value=\"#{k.to_s}\" #{selected}>#{Contact::NUMBER_TYPES[k]}</option>"
      end
    html += "</select>"

    raw(html)
  end
end
