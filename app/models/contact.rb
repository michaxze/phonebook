class Contact < ActiveRecord::Base
  validates :numbers, presence: true
  validates :name, presence: true
  validates :name, uniqueness: true
  serialize :numbers
  before_create :set_user_id

  self.per_page = 4
  USER_ID = 1
  NUMBER_TYPES = {
    :home   => "Home",
    :mobile => "Mobile",
    :office => "Office"
  }

  class << self
    def delete_contact(id)
      contact = Contact.find(id)
      return nil if contact.nil?
      
      contact.destroy 
      contact      
    end

    def update_contact(id, contact)
      rec = Contact.find(id)
      return nil if rec.nil?
      
      numbers = {}
        contact[:number_type].each_with_index do |typ, indx|
        numbers["#{typ}"] = contact[:number][indx]
      end

      rec.name = contact[:name]
      rec.numbers = numbers
      rec.save
      rec
    end
    
    def create_contact(name, number)
      contact = Contact.new(:name => name, :numbers => { :mobile => number })
      contact.save
      contact
    end
    
    def get_records(page=1, search=nil)
      page = page.to_i unless page.nil?
      results = Contact.where("user_id = ?", USER_ID)

      unless search.nil?
        results = results.where("LOWER(name) LIKE ?", "#{search.downcase}%")
      end

      results.page(page).order("name ASC")
    end

    def upload(file)
        spreadsheet = Roo::CSV.new(file.path)
        contacts = Contact.where("user_id = ?", USER_ID)
        new_names = []
        
        (2..spreadsheet.last_row).each do |i|
          name = spreadsheet.cell(i, 1).to_s.strip
          numbers = spreadsheet.cell(i, 2).to_s.strip.split(";")
          new_names << name.downcase
          
          Contact.create_from_csv(name, numbers)
        end
        
        #cleaning...
        unless new_names.empty?
          contacts.each do |contact|
            contact.destroy if !new_names.include? contact.name.downcase.strip
          end
        end
    end
    
    def to_csv(options = {})
      require 'csv'

      CSV.generate(options) do |csv|
        csv << ["Name", "Numbers"]
        all.each do |contact|
          num = []
          row = []
          row << contact.name

          contact.numbers.each_key do |k|
            num << "#{k.to_s} : " + contact.numbers[k].to_s
          end
          row << num.join(";")
          csv << row
        end
      end    
    end
    
    #numbers = ["mobile : 0142323444", " office: 032434344"]
    def create_from_csv(name, numbers)
      num_hash = {}
      records = where("LOWER(name) = ?", name.to_s.downcase.strip)

      numbers.each do |no|
        ntype, number = no.split(":")
        num_hash[ntype.downcase.strip.to_sym] = number.to_s.strip
      end
      
      if records.empty?
        Contact.create(:name => name.to_s.strip, :numbers => num_hash)
      else
        records.each do |contact|
          contact.update_contact_numbers(numbers)
        end
      end
    end

  end

  def primary_number
    return "" if numbers.nil? || numbers.empty?
    numbers[numbers.keys.first]
  end
  
  def update_contact_numbers(new_numbers=[])
    return if new_numbers.empty?
    num_hash = {}
    
    new_numbers.each do |no|
      ntype, number = no.split(":")
      num_hash[ntype.downcase.strip.to_sym] = number.to_s.strip
    end

    self.update_attribute(:numbers, num_hash) unless num_hash.empty?
  end
  
  def set_user_id
      self.user_id = USER_ID
  end
end
