module Api
  class ContactsController < ApplicationController
    respond_to :json

    def index
      respond_with Contact.get_records(params[:page])
    end

    def show
      respond_with Contact.find(params[:id])
    end

    def create
      respond_with Contact.create_contact(params[:contact])
    end

    def update
      respond_with Contact.update_contact(params[:id], params[:contact])
    end

    def destroy
      respond_with Contact.destroy_contact(params[:id])
    end
  end
end