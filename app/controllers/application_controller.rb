class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  after_filter :set_xhr_flash

  def set_xhr_flash
    flash.discard if request.xhr?
  end
end
