class ContactsController < ApplicationController
  respond_to :json

  def upload    
    if request.post?
      Contact.upload(params[:file])
      redirect_to root_url, :notice => "Contacts successfully updated."
    end
  end
  
  def download
    @contacts = Contact.where("user_id=?", Contact::USER_ID)

    respond_to do |format|
      format.csv { send_data @contacts.to_csv }
    end
  end
  
  def index
    @contacts = Contact.get_records(params[:page], params[:q])
  end

  def edit
    @contact = Contact.find(params[:id])
  end
  
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.create_contact(params[:contact][:name], params[:number])
  end

  def update
    @contact = Contact.update_contact(params[:id], params[:contact])
  end
  
  def destroy
    @contact = Contact.delete_contact(params[:id])
  end
end
